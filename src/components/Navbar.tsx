import React from 'react'
import { Navbar, Container, Nav } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import { HOME_ROUTE } from '../utils/consts';

function NavBar() {
    const history = useNavigate()
  return (
    <Navbar bg="dark" variant="dark">
        <Container>
            <Navbar.Brand className='text-success'>REACT-REDUX-TS</Navbar.Brand>
            <Nav className="mr-auto" >
                <Nav.Link onClick={() => history(HOME_ROUTE)}>Home</Nav.Link>
            </Nav>
        </Container>
    </Navbar>
  )
}

export default NavBar