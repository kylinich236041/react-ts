import React from 'react'
import { Navigate, Route, Routes } from 'react-router-dom'
import { publicRoutes } from '../routes'
import { HOME_ROUTE } from '../utils/consts'

function AppRouter() {
  return (
    <Routes>
        {publicRoutes.map(({path, Component}) => 
          <Route key={path}  path={path} element={<Component />} />
        )}
        <Route path="*" element={<Navigate replace to={HOME_ROUTE} />} />
    </Routes>
  )
}

export default AppRouter