import React from 'react'
import { Container } from 'react-bootstrap'
import { useLocation, useNavigate } from 'react-router-dom'
import { SIGNIN_ROUTE } from '../utils/consts'

function Auth() {
  const location = useLocation()
//   /const history = useNavigate()
  const isSignin = location.pathname === SIGNIN_ROUTE
  return (
    <Container
    className="d-flex justify-content-center align-items-center"
    style={{height: window.innerHeight - 54}}
    >
        {isSignin ? <div>signin</div> : <div>signup</div>}
    </Container>
  )
}

export default Auth