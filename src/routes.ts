import Auth from "./pages/Auth"
import Home from "./pages/Home"
import { HOME_ROUTE, SIGNIN_ROUTE, SIGNUP_ROUTE } from "./utils/consts"


export const adminRoutes = [
]

export const publicRoutes = [
    {
        path: HOME_ROUTE,
        Component: Home
    },
    {
        path: SIGNIN_ROUTE,
        Component: Auth
    },
    {
        path: SIGNUP_ROUTE,
        Component: Auth
    }
]